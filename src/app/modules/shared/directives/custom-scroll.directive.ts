import { Directive, ElementRef, EventEmitter, Input, NgZone, Output, SimpleChanges } from "@angular/core";
import { AbstractClassOnDestroy } from '@shared/abstractOnDestroy-class';
import { fromEvent } from "rxjs";
import { debounceTime, filter, takeUntil } from "rxjs/operators";

@Directive({
  selector: '[infiniteScroll]'
})
export class CustomScrollDirective extends AbstractClassOnDestroy  {

  @Output() richTheButtom = new EventEmitter<void>()
  @Input() listView: boolean
  previousScrollPosition: number = 0

  constructor(private el: ElementRef, private ngZone: NgZone) { super()}

  ngOnInit() {
    this.ngZone.runOutsideAngular(() => {
      this.detectingScrolledPosition()
    })
  }

  private detectingScrolledPosition(): void {
    const hostElFixedHeight = parseInt(getComputedStyle(this.el.nativeElement).height)
    fromEvent(this.el.nativeElement, "scroll").pipe(
      debounceTime(300),
      filter((e: Event) => (e.target as Element).scrollTop > this.previousScrollPosition),
      takeUntil(this.onDestroy$)
    )
      .subscribe((e: Event) => {
        this.previousScrollPosition = (e.target as Element).scrollTop
        const scrolledAreaHeight = (e.target as Element).scrollTop
        const hostElementContentHeight = this.el.nativeElement.scrollHeight
        if ((hostElFixedHeight + scrolledAreaHeight) / hostElementContentHeight > 0.9) this.emitEvent()
      })
  }

  private emitEvent(): void {
    this.ngZone.run(() => {
      this.richTheButtom.emit();
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.listView.currentValue) {
      this.previousScrollPosition = 0
    }
  }
}



