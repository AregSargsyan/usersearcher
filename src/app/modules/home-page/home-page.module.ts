import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageRoutingModule } from './home-page-routing.module';
import { HomePageComponent } from './home-page.component';
import { SearchUserComponent } from './search-user/search-user.component';
import { PopularUsersComponent } from './popular-users/popular-users.component';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [HomePageComponent, SearchUserComponent, PopularUsersComponent],
  imports: [
    CommonModule,
    HomePageRoutingModule,
    SharedModule
  ]
})
export class HomePageModule { }
