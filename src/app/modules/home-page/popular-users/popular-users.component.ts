import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { shareReplay, tap } from 'rxjs/operators'
import { Repository } from 'src/app/interfaces/repo.interface'
import { UserInfo } from 'src/app/interfaces/userInfo.interface'
import { UserService } from 'src/app/services/user.service'

export interface UserViewInfo {
  userUrl: string
  reposUrl: string
  viewType: boolean
}

@Component({
  selector: 'popular-users',
  templateUrl: './popular-users.component.html',
  styleUrls: ['./popular-users.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopularUsersComponent implements OnInit {

  @Input() userViewInfo: UserViewInfo

  userInfo$: Observable<UserInfo>
  userReposInfo$: Observable<Repository[]>

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUserData()
    this.getUserRepos()
  }

  private getUserData(): void {
    this.userInfo$ = this.userService.getUserInfo(this.userViewInfo.userUrl).pipe(
      shareReplay()
    )
  }

  private getUserRepos(): void {
    this.userReposInfo$ = this.userService.getrepos(this.userViewInfo.reposUrl).pipe(
      shareReplay()
    )
  }
}
