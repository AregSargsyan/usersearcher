import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', pathMatch: 'full', loadChildren: () => import('@homePage/home-page.module').then(m => m.HomePageModule), data: { animation: 'HomePage' } },
  { path: ':user', loadChildren: () => import('@userInfo/user-info.module').then(m => m.UserInfoModule), data: { animation: 'UserDetails' } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
