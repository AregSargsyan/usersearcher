import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';
import { PopularUser } from 'src/app/interfaces/popularUsers.interface';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePageComponent implements OnInit {

  listView: boolean = true
  popularUsers: PopularUser[] = []
  pageNumber: number = 1

  constructor(private userService: UserService, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getUsers()
  }

  changeViewType(viewType: boolean):void {
    this.listView = viewType
  }

  private getUsers(): void {
    this.userService.getUsers(this.pageNumber).pipe(
      tap((popularUsers: PopularUser[]) => {
        this.popularUsers.push(...popularUsers)
        this.pageNumber++
        this.cdr.detectChanges()
      })
    ).subscribe()
  }

  richTheButtom(): void {
    this.getUsers()
  }
}
