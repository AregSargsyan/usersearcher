import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import {  filter, shareReplay } from 'rxjs/operators';
import { PopularUser } from 'src/app/interfaces/popularUsers.interface';
import { UserService } from 'src/app/services/user.service';
import { switchMap } from 'rxjs/operators';
import { UserInfo } from 'src/app/interfaces/userInfo.interface';
import { Repository } from 'src/app/interfaces/repo.interface';
import { Organization } from 'src/app/interfaces/organizations.interface';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  userName: string
  userNotFound: boolean = false
  userData: Observable<[UserInfo, Repository[], Organization[]]>

  constructor(private route: Router, private router: ActivatedRoute, private userService: UserService, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.userName = this.router.snapshot.params.user
    this.getUserInfoByName()
  }

  navigateBack() {
    this.route.navigate([''])
  }

  private getUserInfoByName(): void {
    this.userData = this.userService.getUserByName(this.userName).pipe(
      filter(user => {
        if (!user) {
          this.userNotFound = true
          this.cdr.detectChanges()
          return false
        }
        return true
      }),
      switchMap((user: PopularUser) => {
        return forkJoin([
          this.userService.getUserInfo(user.url),
          this.userService.getrepos(user.repos_url),
          this.userService.getUserOrganizations(user.organizations_url)
        ])
      }),
      shareReplay(),
    )
  }
}
