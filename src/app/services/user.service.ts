import { HttpClient,  HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { PopularUser, PopularUsersData } from '../interfaces/popularUsers.interface'
import { map,  } from 'rxjs/operators'
import { UserInfo } from '../interfaces/userInfo.interface'
import { Repository } from '../interfaces/repo.interface'
import { Organization } from '../interfaces/organizations.interface'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  GITHUB_USERS_URL = "https://api.github.com/search/users"

  constructor(private http: HttpClient) { }

  getUsers(page: number): Observable<PopularUser[]> {
    let params = new HttpParams()
    params = params.set('q', 'followers:>=0')
    params = params.set('sort', 'followers')
    params = params.set('order', 'dsc')
    params = params.set('per_page', '10')
    params = params.set('page', `${page}`)
    return this.http.get<PopularUsersData>(this.GITHUB_USERS_URL, { params }).pipe(
      map((popularUsersData: PopularUsersData) => popularUsersData.items)
    )
  }

  getUserInfo(url: string): Observable<UserInfo> {
    return this.http.get<UserInfo>(url).pipe(
      map((userInfo: UserInfo) => {
        userInfo.navigationLink = (userInfo.name || userInfo.login)
        userInfo.name = userInfo.name || userInfo.login
        return userInfo
      })
    )
  }

  getrepos(url: string): Observable<Repository[]> {
    let params = new HttpParams()
    params = params.set('per_page', '3')
    params = params.set('page', '1')
    return this.http.get<Repository[]>(url, { params })
  }

  getUserByName(name: string): Observable<PopularUser> {
    let params = new HttpParams()
    params = params.set('q', `${name}`)
    params = params.set('per_page', '1')

    return this.http.get<PopularUsersData>(this.GITHUB_USERS_URL, { params }).pipe(
      map((userData: PopularUsersData) => userData.items[0])
    )
  }

  getUserOrganizations(url: string): Observable<Organization[]> {
    return this.http.get<Organization[]>(url)
  }
}
