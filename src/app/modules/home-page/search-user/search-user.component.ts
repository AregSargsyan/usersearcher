import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'search-user',
  templateUrl: './search-user.component.html',
  styleUrls: ['./search-user.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class SearchUserComponent {

  previousSerches: string[] = JSON.parse(localStorage.getItem('lastSearches')) || []

  constructor(private router: Router) { }

  searchUser(input: string): void {
    input = input.replace(/\s/g, '')
    if (input) {
      this.previousSerches.push(input)
      if (this.previousSerches.length == 4) this.previousSerches.shift()
      localStorage.setItem('lastSearches', JSON.stringify(this.previousSerches))
      this.router.navigate(['', input])
    }
  }

  searchUserFromList(username: string): void {
    this.router.navigate(['', username])
  }

}
