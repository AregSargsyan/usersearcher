import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

export abstract class AbstractClassOnDestroy implements OnDestroy {

  onDestroy$ = new Subject<void>()

  constructor() { }
  ngOnDestroy(): void {
    this.onDestroy$.next()
    this.onDestroy$.complete()
  }
}
