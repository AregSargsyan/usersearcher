import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()

export class UserInterceptor implements HttpInterceptor {

  constructor() { }



  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const interceptedRequest = req.clone({
      headers: req.headers.set("Authorization", "token 9caa8df696c32dc749a50cc85d2e24ad8cd514b9"),
    })
    return next.handle(interceptedRequest)
  }
}

