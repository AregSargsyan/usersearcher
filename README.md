#GithubUserSearcher

SPA for displaying and searching GitHub users using GitHub public API. Works with custom infinite scroll directive (scroll the container to the bottom will fetch new users)


## Demo
[DEMO](https://authentification-b6a0a.web.app)

# GithubSearcher

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


